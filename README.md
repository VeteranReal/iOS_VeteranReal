# Instructions for Veteran Real

---

Link Requires a GitLab Account. Please note you must first register and sign in for the above like to work. An Email invite from Gitlab to your Fullsail Email was sent with atonal instructions.

** Account Sign In Required for Link to work **

** DATABASE WIPED NEW LOGIN REQUIRED

**GitLab Repository Link**

Repository Link - https://gitlab.com/VeteranReal/iOS_VeteranReal.git

Please note you must first register and sign in for the above like to work. An Email invite from Gitlab to your Fullsail Email was sent with atonal instructions.

**Software Required** 

Xcode 10
Xcode Simulator Version 9.4.1 (Phone X)
iOS Build Target 11.4
App Must be run in Simulator 10 using iPhone X

**Pod files required for installation.**

pod 'Firebase/Core'
pod 'Firebase/Auth'
pod 'Firebase/Database'
pod 'Firebase/Storage'
pod 'SDWebImage'
pod 'KILabel'
pod 'ImagePicker'

**To** Run Project. Open using WorkSpace

VeteranRealApp.xcworkspace

**User Test Account**

    Name : Matt Sea
    Email : mattsea@email.com
    Password : password
	
    Name : Peter White
    Email : Peterwhite@email.com
    Password : password

