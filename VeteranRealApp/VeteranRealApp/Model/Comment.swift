//
//  Comment.swift
//  VeteranRealApp
//
//  Created by James Calby on 10/11/18.
//  Copyright © 2018 VeteranReal. All rights reserved.
//

import Foundation
import Firebase

class Comment {
    
    var commentText: String?
    var uid: String?
    
    init(dictionary: [String : Any]) {
        self.commentText = dictionary["commentText"] as? String ?? ""
        self.uid = dictionary["uid"] as? String ?? ""
    }
}

