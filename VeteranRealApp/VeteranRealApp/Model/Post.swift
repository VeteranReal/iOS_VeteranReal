//
//  Post.swift
//  VeteranRealApp
//
//  Created by James Calby on 10/11/18.
//  Copyright © 2018 VeteranReal. All rights reserved.
//

import Foundation
import Firebase

class Post {
    
    var caption: String?
    var photoUrl: String?
    var uid: String?
    var id: String?
    var likeCount: Int?
    var likes: Dictionary<String , Any>?
    var isLiked: Bool?
    var ratio: CGFloat?
    var videoUrl: String?
    var timestamp: Int?
    
    init(dictionary: [String : Any], key: String) {
        self.caption = dictionary["caption"] as? String ?? ""
        self.photoUrl = dictionary["photoUrl"] as? String ?? ""
        self.uid = dictionary["uid"] as? String ?? ""
        self.id = key
        self.likeCount = dictionary["likeCount"] as? Int ?? 0
        self.likes = dictionary["likes"] as? Dictionary<String , Any> ?? [:]
        self.ratio = dictionary["ratio"] as? CGFloat
        self.videoUrl = dictionary["videoUrl"] as? String ?? ""
        self.timestamp = dictionary["timestamp"] as? Int
        
        if let currentUserId = Auth.auth().currentUser?.uid {
            if self.likes != nil {
                self.isLiked = self.likes![currentUserId] != nil
            }
        }
    }
}
