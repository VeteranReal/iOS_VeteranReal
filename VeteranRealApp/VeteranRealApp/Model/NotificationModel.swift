//
//  NotificationModel.swift
//  VeteranRealApp
//
//  Created by James Calby on 10/11/18.
//  Copyright © 2018 VeteranReal. All rights reserved.
//

import Foundation
import Firebase

class NotificationModel {
    var from: String?
    var objectId: String?
    var type: String?
    var timestamp: Int?
    var id: String?
    
    
    init(dictionary: [String : Any], key: String) {
        self.from = dictionary["from"] as? String ?? ""
        self.objectId = dictionary["objectId"] as? String ?? ""
        self.type = dictionary["type"] as? String ?? ""
        self.id = key
        self.timestamp = dictionary["timestamp"] as? Int
    }
}
