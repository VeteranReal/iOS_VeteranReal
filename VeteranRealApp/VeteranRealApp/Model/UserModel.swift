//
//  UserModel.swift
//  VeteranRealApp
//
//  Created by James Calby on 10/11/18.
//  Copyright © 2018 VeteranReal. All rights reserved.
//

import Foundation

class UserModel {
    var email: String?
    var profileImageUrl: String?
    var fullName: String?
    var id: String?
    var isFollowing: Bool?
    
    init(dictionary: [String : Any], key: String) {
        self.email = dictionary["email"] as? String ?? ""
        self.profileImageUrl = dictionary["profileImageUrl"] as? String ?? ""
        self.fullName = dictionary["fullname"] as? String ?? ""
        self.id = key
    }
    
}
