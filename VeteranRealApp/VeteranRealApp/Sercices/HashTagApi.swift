//
//  HashTagApi.swift
//  VeteranRealApp
//
//  Created by James Calby on 10/10/18.
//  Copyright © 2018 VeteranReal. All rights reserved.
//

import Foundation
import Firebase

class HashTagApi {
    var REF_HASHTAG = Database.database().reference().child("hashTag")
    
    func fetchPosts(withTag tag: String, completion: @escaping(String) -> ()) {
        REF_HASHTAG.child(tag.lowercased()).observe(.childAdded) { (snapshot) in
            completion(snapshot.key)
        }
    }
}

