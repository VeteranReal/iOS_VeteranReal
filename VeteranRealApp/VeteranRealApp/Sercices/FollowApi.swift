//
//  FollowApi.swift
//  VeteranRealApp
//
//  Created by James Calby on 10/10/18.
//  Copyright © 2018 VeteranReal. All rights reserved.
//

import Foundation
import Firebase

class FollowApi {
    
    var REF_FOLLOWERS = Database.database().reference().child("followers")
    var REF_FOLLOWING = Database.database().reference().child("following")
    
    func followAction(withUser id: String) {
        guard let currentUserId = Api.User.CURRENT_USER?.uid else { return }
        Api.MyPosts.REF_MY_POSTS.child(id).observeSingleEvent(of: .value) { (snapshot) in
            if let dictionaries = snapshot.value as? [String : Any] {
                for key in dictionaries.keys {
                    if let value = dictionaries[key] as? [String : Any]{
                        let timestampPost = value["timestamp"] as! Int
                        Database.database().reference().child("feed").child(currentUserId).child(key).setValue(["timestamp" : timestampPost])
                    }
                }
            }
        }
        REF_FOLLOWERS.child(id).child(currentUserId).setValue(true)
        REF_FOLLOWING.child(currentUserId).child(id).setValue(true)
        let timestamp = NSNumber(value: Int(Date().timeIntervalSince1970))
        let newNotificationRef = Api.Notification.REF_NOTIFICATION.child(id).child("\(id)-\(currentUserId)")
        newNotificationRef.setValue(["from": currentUserId, "objectId": currentUserId, "type": "follow", "timestamp": timestamp])
    }
    
    func unFollowAction(withUser id: String) {
        guard let currentUserId = Api.User.CURRENT_USER?.uid else { return }
        Api.MyPosts.REF_MY_POSTS.child(id).observeSingleEvent(of: .value) { (snapshot) in
            if let dictionaries = snapshot.value as? [String : Any] {
                for key in dictionaries.keys {
                    Database.database().reference().child("feed").child(currentUserId).child(key).removeValue()
                }
            }
        }
        REF_FOLLOWERS.child(id).child(currentUserId).setValue(NSNull())
        REF_FOLLOWING.child(currentUserId).child(id).setValue(NSNull())
        let newNotificationRef = Api.Notification.REF_NOTIFICATION.child(id).child("\(id)-\(currentUserId)")
        newNotificationRef.setValue(NSNull())
    }
    
    func isFollowing(userId: String, completed: @escaping (Bool) -> ()) {
        guard let currentUserId = Api.User.CURRENT_USER?.uid else { return }
        REF_FOLLOWERS.child(userId).child(currentUserId).observeSingleEvent(of: .value) { (snapshot) in
            if let _ = snapshot.value as? NSNull {
                completed(false)
            } else {
                completed(true)
            }
        }
    }
    
    func fetchCountFollowing(userId: String, completion: @escaping (Int) -> ()) {
        REF_FOLLOWING.child(userId).observe(.value) { (snapshot) in
            let count = Int(snapshot.childrenCount)
            completion(count)
        }
    }
    
    func fetchCountFollowers(userId: String, completion: @escaping (Int) -> ()) {
        REF_FOLLOWERS.child(userId).observe(.value) { (snapshot) in
            let count = Int(snapshot.childrenCount)
            completion(count)
        }
    }
    
}



