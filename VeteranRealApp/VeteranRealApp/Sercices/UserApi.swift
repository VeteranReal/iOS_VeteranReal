//
//  UserApi.swift
//  VeteranRealApp
//
//  Created by James Calby on 10/10/18.
//  Copyright © 2018 VeteranReal. All rights reserved.
//

import Foundation
import Firebase

class UserApi {
    var REF_USERS = Database.database().reference().child("users")
    
    func observeUserByFullName(fullName: String, completion: @escaping (UserModel) -> ()) {
        REF_USERS.queryOrdered(byChild: "fullname_lowercase").queryEqual(toValue: fullName).observeSingleEvent(of: .childAdded) { (snapshot) in
            guard let dictionaries = snapshot.value as? [String : Any] else { return }
            let user = UserModel(dictionary: dictionaries, key: snapshot.key)
            completion(user)
        }
    }
    
    func observeUser(withId uid: String, completion: @escaping (UserModel) -> ()) {
        REF_USERS.child(uid).observeSingleEvent(of: .value) { (snapshot) in
            guard let dictionaries = snapshot.value as? [String : Any] else { return }
            let user = UserModel(dictionary: dictionaries, key: snapshot.key)
            completion(user)
        }
    }
    
    func observeCurrentUser(completion: @escaping (UserModel) -> ()) {
        guard let currentUser = Auth.auth().currentUser else { return }
        REF_USERS.child(currentUser.uid).observeSingleEvent(of: .value) { (snapshot) in
            guard let dictionaries = snapshot.value as? [String : Any] else { return }
            let user = UserModel(dictionary: dictionaries, key: snapshot.key)
            completion(user)
        }
    }
    
    func observeUsers(completion: @escaping (UserModel) -> ()) {
        REF_USERS.observe(.childAdded) { (snapshot) in
            guard let dictionaries = snapshot.value as? [String : Any] else { return }
            let user = UserModel(dictionary: dictionaries, key: snapshot.key)
            guard let id = user.id else { return }
            if id != Api.User.CURRENT_USER?.uid {
                completion(user)
            }
        }
    }
    
    func queryUsers(withText text: String, completion: @escaping (UserModel) -> ()) {
        REF_USERS.queryOrdered(byChild: "fullname_lowercase").queryStarting(atValue: text).queryEnding(atValue: text+"\u{f8ff}").queryLimited(toFirst: 10).observeSingleEvent(of: .value) { (snapshot) in
            snapshot.children.forEach({ (s) in
                let child = s as! DataSnapshot
                guard let dictionaries = child.value as? [String : Any] else { return }
                let user = UserModel(dictionary: dictionaries, key: child.key)
                completion(user)
            })
        }
    }
    
    var CURRENT_USER: User? {
        if let currentUser = Auth.auth().currentUser {
            return currentUser
        }
        return nil
    }
    
    var REF_CURRENT_USER: DatabaseReference? {
        guard let currentUser = Auth.auth().currentUser else { return nil }
        return REF_USERS.child(currentUser.uid)
    }
}
