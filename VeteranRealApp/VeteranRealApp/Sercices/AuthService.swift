//
//  AuthService.swift
//  VeteranRealApp
//
//  Created by James Calby on 10/10/18.
//  Copyright © 2018 VeteranReal. All rights reserved.
//

import Foundation
import UIKit
import Firebase

class AuthService {
    static func signIn(email: String, password: String, onSuccess: @escaping () -> (), onError: @escaping (_ errorMessage: String) -> ()) {
        Auth.auth().signIn(withEmail: email, password: password) { (user, err) in
            if err != nil {
                onError(err?.localizedDescription ?? "")
                return
            }
            onSuccess()
        }
    }
    
    static func signUp(fullName: String, email: String, password: String, imageData: Data, onSuccess: @escaping () -> (), onError: @escaping (_ errorMessage: String) -> ()) {
        Auth.auth().createUser(withEmail: email, password: password) { (user, err) in
            if err != nil {
                onError(err?.localizedDescription ?? "")
                return
            }
            // guard let uid = user?.uid else { return }
            let uid = user!.user.uid
            let storageRef = Storage.storage().reference().child("profile_image").child(uid)
            storageRef.putData(imageData, metadata: nil, completion: { (metadata, err) in
                if let err = err {
                    print("ERROR: upload profile image: ", err)
                    return
                }
                storageRef.downloadURL(completion: { (url, error) in
                    if let err = error {
                        print("ERROR: retrieve URL for profile image:", err)
                        return
                    }
                    guard let profileImageUrl = url?.absoluteString else { return }
                    self.setUserInfo(profileImageUrl: profileImageUrl, fullName: fullName, email: email, uid: uid, onSuccess: onSuccess)
                })
            })
        }
    }
    
    static func setUserInfo(profileImageUrl: String, fullName: String, email: String, uid: String, onSuccess: @escaping () -> ()) {
        let dictionary = ["fullname" : fullName, "fullname_lowercase" : fullName.lowercased(), "email" : email, "profileImageUrl" : profileImageUrl]
        let ref = Database.database().reference().child("users").child(uid)
        ref.setValue(dictionary, withCompletionBlock: { (err, _) in
            if let err = err {
                print("ERROR: save user info into database:", err)
                return
            }
            print("Successfully saved user's info to database")
            onSuccess()
        })
    }
    
    static func updateUserInfo(fullName: String, email: String, imageData: Data, onSuccess: @escaping () -> (), onError: @escaping (_ errorMessage: String) -> ()) {
        Api.User.CURRENT_USER?.updateEmail(to: email, completion: { (err) in
            if let err = err {
                onError(err.localizedDescription)
            }
            guard let currentUserId = Api.User.CURRENT_USER?.uid else { return }
            let storageRef = Storage.storage().reference().child("profile_image").child(currentUserId)
            storageRef.putData(imageData, metadata: nil, completion: { (metadata, err) in
                if let err = err {
                    print("ERROR: upload profile image: ", err)
                    return
                }
                storageRef.downloadURL(completion: { (url, error) in
                    if let err = error {
                        print("ERROR: retrieve URL for profile image:", err)
                        return
                    }
                    guard let profileImageUrl = url?.absoluteString else { return }
                    self.updateDatabase(profileImageUrl: profileImageUrl, fullName: fullName, email: email, onSuccess: onSuccess, onError: onError)
                })
            })
        })
    }
    
    static func updateDatabase(profileImageUrl: String, fullName: String, email: String, onSuccess: @escaping () -> (), onError: @escaping (_ errorMessage: String) -> ()) {
        let dictionary = ["fullname" : fullName, "fullname_lowercase" : fullName.lowercased(), "email" : email, "profileImageUrl" : profileImageUrl]
        Api.User.REF_CURRENT_USER?.updateChildValues(dictionary, withCompletionBlock: { (err, ref) in
            if let err = err {
                onError(err.localizedDescription)
            }
            onSuccess()
        })
    }
    
    static func logOut(onSuccess: @escaping () -> (), onError: @escaping (_ errorMessage: String) -> ()) {
        do {
            try Auth.auth().signOut()
            onSuccess()
        } catch let logoutError {
            onError(logoutError.localizedDescription)
        }
    }
}

