//
//  CommentApi.swift
//  VeteranRealApp
//
//  Created by James Calby on 10/10/18.
//  Copyright © 2018 VeteranReal. All rights reserved.
//

import Foundation
import Firebase

class CommentApi {
    var REF_COMMENTS = Database.database().reference().child("comments")
    func observeComments(withPostId id: String, completion: @escaping (Comment) -> ()) {
        REF_COMMENTS.child(id).observeSingleEvent(of: .value) { (snapshot) in
            guard let dictionaries = snapshot.value as? [String : Any] else { return }
            let comment = Comment(dictionary: dictionaries)
            completion(comment)
        }
    }
}
