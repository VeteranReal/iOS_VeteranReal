//
//  NotificationApi.swift
//  VeteranRealApp
//
//  Created by James Calby on 10/10/18.
//  Copyright © 2018 VeteranReal. All rights reserved.
//

import Foundation
import Firebase

class NotificationApi {
    var REF_NOTIFICATION = Database.database().reference().child("notification")
    
    func observeNotification(withId id: String, completion: @escaping (NotificationModel) -> ()) {
        REF_NOTIFICATION.child(id).observe(.childAdded) { (snapshot) in
            if let dictionary = snapshot.value as? [String : Any] {
                let notification = NotificationModel(dictionary: dictionary, key: snapshot.key)
                completion(notification)
            }
        }
    }
    
}
