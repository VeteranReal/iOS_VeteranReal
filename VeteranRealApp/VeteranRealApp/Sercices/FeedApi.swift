//
//  FeedApi.swift
//  VeteranRealApp
//
//  Created by James Calby on 10/10/18.
//  Copyright © 2018 VeteranReal. All rights reserved.
//

import Foundation
import Firebase

class FeedApi {
    
    var REF_FEED = Database.database().reference().child("feed")
    
    func observeFeed(withId id: String, completion: @escaping (Post) -> ()) {
        REF_FEED.child(id).queryOrdered(byChild: "timestamp").observe(.childAdded) { (snapshot) in
            let key = snapshot.key
            Api.Post.observePost(withId: key, completion: { (post) in
                completion(post)
            })
        }
    }
    
    func getRecentFeed(withId id: String, start timestamp: Int? = nil, limit: UInt, completion: @escaping ([(Post, UserModel)]) -> ()) {
        var feedQuery = REF_FEED.child(id).queryOrdered(byChild: "timestamp")
        if let latestPostTimestamp = timestamp, latestPostTimestamp > 0 {
            feedQuery = feedQuery.queryStarting(atValue: latestPostTimestamp + 1, childKey: "timestamp").queryLimited(toLast: limit)
        } else {
            feedQuery = feedQuery.queryLimited(toLast: limit)
        }
        var results: [(post: Post, user: UserModel)] = []
        feedQuery.observeSingleEvent(of: .value) { (snapshot) in
            let items = snapshot.children.allObjects as! [DataSnapshot]
            let myGroup = DispatchGroup()
            for (_, item) in items.enumerated() {
                myGroup.enter()
                Api.Post.observePost(withId: item.key, completion: { (post) in
                    guard let postUid = post.uid else { return }
                    Api.User.observeUser(withId: postUid, completion: { (user) in
                        if postUid == user.id
                        {
                            results.append((post, user))
                        }
                        //results.insert((post, user), at: index)
                        myGroup.leave()
                    })
                })
            }
            myGroup.notify(queue: .main, execute: {
                results.sort(by: { $0.0.timestamp! > $1.0.timestamp!})
                completion(results)
            })
        }
    }
    
    func getOldFeed(withId id: String, start timestamp: Int, limit: UInt, completion: @escaping ([(Post, UserModel)]) -> ()) {
        let feedOlderQuery = REF_FEED.child(id).queryOrdered(byChild: "timestamp")
        let feedLimitedQuery = feedOlderQuery.queryEnding(atValue: timestamp - 1, childKey: "timestamp").queryLimited(toLast: limit)
        feedLimitedQuery.observeSingleEvent(of: .value) { (snapshot) in
            let items = snapshot.children.allObjects as! [DataSnapshot]
            let myGroup = DispatchGroup()
            var results: [(post: Post, user: UserModel)] = []
            for (index, item) in items.enumerated() {
                myGroup.enter()
                Api.Post.observePost(withId: item.key, completion: { (post) in
                    guard let postUid = post.uid else { return }
                    Api.User.observeUser(withId: postUid, completion: { (user) in
                        results.insert((post, user), at: index)
                        myGroup.leave()
                    })
                })
            }
            myGroup.notify(queue: DispatchQueue.main, execute: {
                results.sort(by: { $0.0.timestamp! > $1.0.timestamp!})
                completion(results)
            })
        }
    }
    
    func observeFeedRemoved(withId id: String, completion: @escaping (Post) -> ()) {
        REF_FEED.child(id).observe(.childRemoved) { (snapshot) in
            let key = snapshot.key
            Api.Post.observePost(withId: key, completion: { (post) in
                completion(post)
            })
        }
        
    }
}

