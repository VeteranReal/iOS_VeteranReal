//
//  MyPostsApi.swift
//  VeteranRealApp
//
//  Created by James Calby on 10/10/18.
//  Copyright © 2018 VeteranReal. All rights reserved.
//

import Foundation
import Firebase

class MyPostsApi {
    var REF_MY_POSTS = Database.database().reference().child("myPosts")
    
    func fetchMyPosts(userId: String, completion: @escaping (String) -> ()) {
        REF_MY_POSTS.child(userId).observe(.childAdded) { (snapshot) in
            completion(snapshot.key)
        }
    }
    
    func fetchCountMyPosts(userId: String, completion: @escaping (Int) -> ()) {
        REF_MY_POSTS.child(userId).observe(.value) { (snapshot) in
            let count = Int(snapshot.childrenCount)
            completion(count)
        }
    }
}
