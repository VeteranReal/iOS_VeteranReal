//
//  Post_CommentApi.swift
//  VeteranRealApp
//
//  Created by James Calby on 10/10/18.
//  Copyright © 2018 VeteranReal. All rights reserved.
//

import Foundation
import Firebase

class Post_CommentApi {
    
    // database testing - posts-comments
    // var REF_POSTS_COMMENTS = Database.database().reference().child("posts-comments")
    var REF_POSTS_COMMENTS = Database.database().reference().child("post-comments")
    
    func observePostsComments(withPostId id: String, completion: @escaping (DataSnapshot) -> ()) {
        REF_POSTS_COMMENTS.child(id).observe(.childAdded) { (snapshot) in
            completion(snapshot)
        }
    }
}
