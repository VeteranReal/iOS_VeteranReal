//
//  PostApi.swift
//  VeteranRealApp
//
//  Created by James Calby on 10/10/18.
//  Copyright © 2018 VeteranReal. All rights reserved.
//

import Foundation
import Firebase

class PostApi {
    var REF_POSTS = Database.database().reference().child("posts")
    
    func observePosts(completion: @escaping (Post) -> ()) {
        REF_POSTS.observe(.childAdded) { (snapshot) in
            guard let dictionaries = snapshot.value as? [String : Any] else { return }
            let post = Post(dictionary: dictionaries, key: snapshot.key)
            completion(post)
        }
    }
    
    func observePost(withId id: String, completion: @escaping (Post) -> ()) {
        REF_POSTS.child(id).observeSingleEvent(of: .value) { (snapshot) in
            guard let dictionaries = snapshot.value as? [String : Any] else { return }
            let post = Post(dictionary: dictionaries, key: snapshot.key)
            completion(post)
        }
    }
    func observeLikeCount(withId id: String, completion: @escaping (Int, UInt) -> ()) {
        var likeHandler: UInt!
        likeHandler = REF_POSTS.child(id).observe(.childChanged) { (snapshot) in
            if let value = snapshot.value as? Int {
                completion(value, likeHandler)
            }
        }
    }
    
    func observeTopPosts(completion: @escaping (Post) -> ()) {
        REF_POSTS.queryOrdered(byChild: "likeCount").observeSingleEvent(of: .value) { (snapshot) in
            let arraySnapshot = (snapshot.children.allObjects as! [DataSnapshot]).reversed()
            arraySnapshot.forEach({ (child) in
                guard let dictionaries = child.value as? [String : Any] else { return }
                let post = Post(dictionary: dictionaries, key: child.key)
                completion(post)
            })
        }
    }
    
    func removeObserveLikeCount(id: String, likeHandler: UInt) {
        Api.Post.REF_POSTS.child(id).removeObserver(withHandle: likeHandler)
    }
    
    func incrementLikes(postId: String, onSuccess: @escaping (Post) -> (), onError: @escaping (_ error: String?) -> ()) {
        let postRef = Api.Post.REF_POSTS.child(postId)
        postRef.runTransactionBlock({ (currentData: MutableData) -> TransactionResult in
            if var post = currentData.value as? [String : AnyObject], let uid = Api.User.CURRENT_USER?.uid {
                var likes: Dictionary<String, Bool>
                likes = post["likes"] as? [String : Bool] ?? [:]
                var likeCount = post["likeCount"] as? Int ?? 0
                if let _ = likes[uid] {
                    likeCount -= 1
                    likes.removeValue(forKey: uid)
                } else {
                    likeCount += 1
                    likes[uid] = true
                }
                post["likeCount"] = likeCount as AnyObject?
                post["likes"] = likes as AnyObject?
                
                currentData.value = post
                
                return TransactionResult.success(withValue: currentData)
            }
            return TransactionResult.success(withValue: currentData)
        }) { (error, committed, snapshot) in
            if let error = error {
                onError(error.localizedDescription)
            }
            if let dictionaries = snapshot?.value as? [String : Any] {
                let post = Post(dictionary: dictionaries, key: snapshot!.key)
                onSuccess(post)
            }
        }
    }
}
