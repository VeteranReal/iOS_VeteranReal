//
//  HelperService.swift
//  VeteranRealApp
//
//  Created by James Calby on 10/10/18.
//  Copyright © 2018 VeteranReal. All rights reserved.
//

import Foundation
import FirebaseStorage
import FirebaseDatabase
import Firebase

class HelperService {
    
    static func uploadDataToStorage(data: Data, videoUrl: URL? = nil, ratio: CGFloat, caption: String, onSuccess: @escaping () -> ()) {
        if let videoUrl = videoUrl {
            self.uploadVideoToStorage(videoUrl: videoUrl) { (videoUrl) in
                uploadImageToStorage(data: data, onSuccess: { (thumbnailVideoPhotoUrlString) in
                    saveDataToDatabase(photoUrl: thumbnailVideoPhotoUrlString, videoUrl: videoUrl, ratio: ratio, caption: caption, onSuccess: onSuccess)
                })
            }
        } else {
            uploadImageToStorage(data: data) { (photoUrl) in
                self.saveDataToDatabase(photoUrl: photoUrl, ratio: ratio, caption: caption, onSuccess: onSuccess)
            }
        }
    }
    
    static func uploadVideoToStorage(videoUrl: URL, onSuccess: @escaping(_ videoUrl: String) -> ()) {
        let videoIdString = NSUUID().uuidString
        let storageRef = Storage.storage().reference().child("posts").child(videoIdString)
        storageRef.putFile(from: videoUrl, metadata: nil) { (metadata, err) in
            if let err = err {
                ProgressHUD.showError(err.localizedDescription)
                return
            }
            storageRef.downloadURL(completion: { (url, error) in
                if let err = error {
                    ProgressHUD.showError(err.localizedDescription)
                    return
                }
                
                if let videoUrl = url?.absoluteString {
                    onSuccess(videoUrl)
                }
            })
        }
    }
    
    static func uploadImageToStorage(data: Data, onSuccess: @escaping (_ photoUrl: String) -> ()) {
        let filename = NSUUID().uuidString
        let storageRef = Storage.storage().reference().child("posts").child(filename)
        storageRef.putData(data, metadata: nil) { (metadata, err) in
            if let err = err {
                ProgressHUD.showError(err.localizedDescription)
                return
            }
            storageRef.downloadURL(completion: { (url, error) in
                if let err = error {
                    ProgressHUD.showError(err.localizedDescription)
                    return
                }
                if let selectedPhotoImageUrl = url?.absoluteString {
                    onSuccess(selectedPhotoImageUrl)
                }
            })
        }
    }
    
    static func saveDataToDatabase(photoUrl: String, videoUrl: String? = nil, ratio: CGFloat, caption: String, onSuccess: @escaping () -> ()) {
        let postId = Api.Post.REF_POSTS.childByAutoId().key
        // let newUserPostRef = Api.Post.REF_POSTS.child(postId)
        let newUserPostRef = Api.Post.REF_POSTS.child(postId!)
        guard let currentUser = Api.User.CURRENT_USER else { return }
        let words = caption.components(separatedBy: CharacterSet.whitespacesAndNewlines)
        for var word in words {
            if word.hasPrefix("#") {
                word = word.trimmingCharacters(in: CharacterSet.punctuationCharacters)
                let newHashTagRef = Api.HashTag.REF_HASHTAG.child(word.lowercased())
                newHashTagRef.updateChildValues([postId: true])
            }
        }
        let timestamp = Int(Date().timeIntervalSince1970)
        var values = ["uid" : currentUser.uid , "photoUrl" : photoUrl, "caption" : caption, "likeCount" : 0, "ratio" : ratio, "timestamp" : timestamp] as [String : Any]
        if let videoUrl = videoUrl {
            values["videoUrl"] = videoUrl
        }
        newUserPostRef.setValue(values, withCompletionBlock: { (err, _) in
            if let err = err {
                ProgressHUD.showError(err.localizedDescription)
                return
            }
            // Api.Feed.REF_FEED.child(currentUser.uid).child(postId).setValue(["timestamp" : timestamp])
            Api.Feed.REF_FEED.child(currentUser.uid).child(postId!).setValue(["timestamp" : timestamp])
            Api.Follow.REF_FOLLOWERS.child(currentUser.uid).observeSingleEvent(of: .value, with: { (snapshot) in
                let arraySnapshot = snapshot.children.allObjects as! [DataSnapshot]
                arraySnapshot.forEach({ (child) in
                    //Api.Feed.REF_FEED.child(child.key).child(postId).setValue(["timestamp" : timestamp])
                    Api.Feed.REF_FEED.child(child.key).child(postId!).setValue(["timestamp" : timestamp])
                    let notificationId = Api.Notification.REF_NOTIFICATION.child(child.key).childByAutoId().key
                    // let newNotificationRef = Api.Notification.REF_NOTIFICATION.child(child.key).child(notificationId)
                    let newNotificationRef = Api.Notification.REF_NOTIFICATION.child(child.key).child(notificationId!)
                    //
                    newNotificationRef.setValue(["from" : currentUser.uid, "type" : "feed", "objectId" : postId, "timestamp" : timestamp])
                })
            })
            //let myPostRef = Api.MyPosts.REF_MY_POSTS.child(currentUser.uid).child(postId)
            let myPostRef = Api.MyPosts.REF_MY_POSTS.child(currentUser.uid).child(postId!)
            myPostRef.setValue(["timestamp" : timestamp], withCompletionBlock: { (error, _) in
                if let err = error {
                    ProgressHUD.showError(err.localizedDescription)
                    return
                }
            })
            ProgressHUD.showSuccess("Success")
            onSuccess()
        })
    }
}
