//
//  PeopleTableViewCell.swift
//  VeteranRealApp
//
//  Created by James Calby on 10/11/18.
//  Copyright © 2018 VeteranReal. All rights reserved.
//

import Foundation
import UIKit

protocol PeopleTableViewCellDelegate {
    func goToProfileVC(userId: String)
}
class PeopleTableViewCell: UITableViewCell {
    
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var fullNameLabel: UILabel!
    @IBOutlet weak var followButton: UIButton!
    
    var delegate: PeopleTableViewCellDelegate?
    
    var user: UserModel? {
        didSet {
            updateView()
        }
    }
    
    fileprivate func updateView() {
        fullNameLabel.text = user?.fullName
        if let imageUrlString = user?.profileImageUrl {
            let imageUrl = URL(string: imageUrlString)
            profileImageView.sd_setImage(with: imageUrl, completed: nil)
        }
        
        guard let isFollowing = user?.isFollowing else { return }
        if isFollowing {
            configureUnFollowButton()
        } else {
            configureFollowButton()
        }
    }
    
    func configureFollowButton() {
        followButton.layer.borderWidth = 1
        followButton.layer.borderColor = UIColor(red: 226/255, green: 228/255, blue: 232/255, alpha: 1).cgColor
        followButton.layer.cornerRadius = 5
        followButton.clipsToBounds = true
        followButton.setTitleColor(.white, for: .normal)
        followButton.backgroundColor = .black
        
        self.followButton.setTitle("Follow", for: .normal)
        followButton.addTarget(self, action: #selector(followAction), for: .touchUpInside)
    }
    
    func configureUnFollowButton() {
        followButton.layer.borderWidth = 1
        followButton.layer.borderColor = UIColor(red: 226/255, green: 228/255, blue: 232/255, alpha: 1).cgColor
        followButton.layer.cornerRadius = 5
        followButton.clipsToBounds = true
        followButton.setTitleColor(.white, for: .normal)
        followButton.backgroundColor = UIColor(red: 69/255, green: 142/255, blue: 255/255, alpha: 1)
        
        self.followButton.setTitle("Following", for: .normal)
        followButton.addTarget(self, action: #selector(unFollowAction), for: .touchUpInside)
        
    }
    
    @objc func followAction() {
        if user?.isFollowing == false {
            guard let userId = user?.id else { return }
            Api.Follow.followAction(withUser: userId)
            configureUnFollowButton()
            user?.isFollowing = true
        }
    }
    
    @objc func unFollowAction() {
        if user?.isFollowing == true {
            guard let userId = user?.id else { return }
            Api.Follow.unFollowAction(withUser: userId)
            configureFollowButton()
            user?.isFollowing = false
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleSelectFullNameLabel))
        fullNameLabel.addGestureRecognizer(tapGesture)
        fullNameLabel.isUserInteractionEnabled = true
    }
    
    @objc func handleSelectFullNameLabel() {
        if let id = user?.id {
            delegate?.goToProfileVC(userId: id)
        }
    }
    
}
