//
//  ActivityTableViewCell.swift
//  VeteranRealApp
//
//  Created by James Calby on 10/11/18.
//  Copyright © 2018 VeteranReal. All rights reserved.
//

import Foundation
import UIKit

protocol ActivityTableViewCellDelegate {
    func goToDetailVC(postId: String)
    func goToProfileVC(userId: String)
    func goToCommentVC(postId: String)
}

class ActivityTableViewCell: UITableViewCell {
    
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var fullNameLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var postImageView: UIImageView!
    
    var delegate: ActivityTableViewCellDelegate?
    
    var notification: NotificationModel? {
        didSet {
            updateView()
        }
    }
    
    var user: UserModel? {
        didSet {
            setupUserInfo()
        }
    }
    
    func updateView() {
        switch notification?.type {
        case "feed":
            descriptionLabel.text = "added a new post"
            guard let objectId = notification?.objectId else { return }
            Api.Post.observePost(withId: objectId) { (post) in
                if let photoUrlString = post.photoUrl {
                    let photoUrl = URL(string: photoUrlString)
                    self.postImageView.sd_setImage(with: photoUrl)
                }
            }
        case "like":
            descriptionLabel.text = "liked your post"
            guard let objectId = notification?.objectId else { return }
            Api.Post.observePost(withId: objectId) { (post) in
                if let photoUrlString = post.photoUrl {
                    let photoUrl = URL(string: photoUrlString)
                    self.postImageView.sd_setImage(with: photoUrl)
                }
            }
        case "comment":
            descriptionLabel.text = "left a comment on your post"
            guard let objectId = notification?.objectId else { return }
            Api.Post.observePost(withId: objectId) { (post) in
                if let photoUrlString = post.photoUrl {
                    let photoUrl = URL(string: photoUrlString)
                    self.postImageView.sd_setImage(with: photoUrl)
                }
            }
        case "follow":
            descriptionLabel.text = "started following you"
            guard let objectId = notification?.objectId else { return }
            Api.Post.observePost(withId: objectId) { (post) in
                if let photoUrlString = post.photoUrl {
                    let photoUrl = URL(string: photoUrlString)
                    self.postImageView.sd_setImage(with: photoUrl)
                }
            }
        default:
            print("t")
        }
        
        if let timestamp = notification?.timestamp {
            print(timestamp)
            let timestampDate = Date(timeIntervalSince1970: Double(timestamp))
            let now = Date()
            let components = Set<Calendar.Component>([.second, .minute, .hour, .day, .weekOfMonth])
            var diff = Calendar.current.dateComponents(components, from: timestampDate, to: now)
            if diff.second! <= 0 {
                timeLabel.text = "Now"
            } else if diff.second! > 0 && diff.minute! == 0 {
                timeLabel.text = "\(diff.second!)s"
            } else if diff.minute! > 0 && diff.hour! == 0 {
                timeLabel.text = "\(diff.minute!)m"
            } else if diff.hour! > 0 && diff.day! == 0 {
                timeLabel.text = "\(diff.hour!)h"
            } else if diff.day! > 0 && diff.weekOfMonth! == 0 {
                timeLabel.text = "\(diff.day!)d"
            } else if diff.weekOfMonth! > 0 {
                timeLabel.text = "\(diff.weekOfMonth!)w"
            }
        }
        
        let tapGestureForCell = UITapGestureRecognizer(target: self, action: #selector(handleSelectForCell))
        addGestureRecognizer(tapGestureForCell)
        isUserInteractionEnabled = true
    }
    
    @objc func handleSelectForCell() {
        if let id = notification?.objectId {
            guard let type = notification?.type else { return }
            if type == "follow" {
                delegate?.goToProfileVC(userId: id)
            } else if type == "comment" {
                delegate?.goToCommentVC(postId: id)
            } else {
                delegate?.goToDetailVC(postId: id)
            }
        }
    }
    
    func setupUserInfo() {
        fullNameLabel.text = user?.fullName
        if let profileUrlString = user?.profileImageUrl {
            let profileUrl = URL(string: profileUrlString)
            self.profileImageView.sd_setImage(with: profileUrl, placeholderImage: UIImage(named: "image2"))
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
