//
//  HomeTableViewCell.swift
//  VeteranRealApp
//
//  Created by James Calby on 10/11/18.
//  Copyright © 2018 VeteranReal. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation
import KILabel

protocol HomeTableViewCellDelegate {
    func goToCommentVC(postId: String)
    func goToProfileUserVC(userId: String)
    func goToHashTag(tag: String)
}

class HomeTableViewCell: UITableViewCell {
    
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var fullNameLabel: UILabel!
    @IBOutlet weak var postImageView: UIImageView!
    @IBOutlet weak var likeImageView: UIImageView!
    @IBOutlet weak var commentImageView: UIImageView!
    @IBOutlet weak var likeCountButton: UIButton!
    @IBOutlet weak var captionLabel: KILabel!
    @IBOutlet weak var heightConstraintOfPostImageView: NSLayoutConstraint!
    @IBOutlet weak var volumeView: UIView!
    @IBOutlet weak var volumeButton: UIButton!
    @IBOutlet weak var timeLabel: UILabel!
    
    var delegate: HomeTableViewCellDelegate?
    var player: AVPlayer?
    var playerLayer: AVPlayerLayer?
    
    var post: Post? {
        didSet {
            updateView()
        }
    }
    
    var user: UserModel? {
        didSet {
            setupUserInfo()
        }
    }
    var isMuted = true
    
    fileprivate func updateView() {
        captionLabel.text = post?.caption
        captionLabel.hashtagLinkTapHandler = { label, string, range in
            let tag = String(string.dropFirst())
            self.delegate?.goToHashTag(tag: tag)
        }
        captionLabel.userHandleLinkTapHandler = { label, string, range in
            let mention = String(string.dropFirst())
            Api.User.observeUserByFullName(fullName: mention.lowercased(), completion: { (user) in
                guard let userId = user.id else { return }
                self.delegate?.goToProfileUserVC(userId: userId)
            })
        }
        
        if let ratio = post?.ratio {
            heightConstraintOfPostImageView.constant = UIScreen.main.bounds.width / ratio
            layoutIfNeeded()
        }
        if let photoUrlString = post?.photoUrl {
            let photoUrl = URL(string: photoUrlString)
            postImageView.sd_setImage(with: photoUrl, completed: nil)
        }
        
        if let videoUrlString = post?.videoUrl, let videoUrl = URL(string: videoUrlString) {
            print("videoUrlString: \(videoUrlString)")
            self.volumeView.isHidden = false
            player = AVPlayer(url: videoUrl)
            playerLayer = AVPlayerLayer(player: player)
            playerLayer?.frame = postImageView.frame
            playerLayer?.frame.size.width = UIScreen.main.bounds.width
            if let ratio = post?.ratio {
                playerLayer?.frame.size.height = UIScreen.main.bounds.width / ratio
            }
            self.contentView.layer.addSublayer(playerLayer!)
            self.volumeView.layer.zPosition = 1
            player?.play()
            player?.isMuted = isMuted
        }
        
        if let timestamp = post?.timestamp {
            print(timestamp)
            let timestampDate = Date(timeIntervalSince1970: Double(timestamp))
            let now = Date()
            let components = Set<Calendar.Component>([.second, .minute, .hour, .day, .weekOfMonth])
            var diff = Calendar.current.dateComponents(components, from: timestampDate, to: now)
            if diff.second! <= 0 {
                timeLabel.text = "Now"
            } else if diff.second! > 0 && diff.minute! == 0 {
                timeLabel.text = (diff.second == 1) ? "\(diff.second!) second ago" : "\(diff.second!) second ago"
            } else if diff.minute! > 0 && diff.hour! == 0 {
                timeLabel.text = (diff.minute == 1) ? "\(diff.minute!) minute ago" : "\(diff.minute!) minute ago"
            } else if diff.hour! > 0 && diff.day! == 0 {
                timeLabel.text = (diff.hour == 1) ? "\(diff.hour!) hour ago" : "\(diff.hour!) hour ago"
            } else if diff.day! > 0 && diff.weekOfMonth! == 0 {
                timeLabel.text = (diff.day == 1) ? "\(diff.day!) day ago" : "\(diff.day!) day ago"
            } else if diff.weekOfMonth! > 0 {
                timeLabel.text = (diff.weekOfMonth == 1) ? "\(diff.weekOfMonth!) week ago" : "\(diff.weekOfMonth!) week ago"
            }
        }
        
        guard let post = self.post else { return }
        self.updateLike(post: post)
        
        NotificationCenter.default.addObserver(self, selector: #selector(stopVideo), name: NSNotification.Name.init("stopVideo"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(playVideo), name: NSNotification.Name.init("playVideo"), object: nil)
    }
    
    @IBAction func volumeButtonDidTapped(_ sender: UIButton) {
        if isMuted {
            isMuted = !isMuted
            volumeButton.setImage(UIImage(named: "Icon_Volume"), for: .normal)
        } else {
            isMuted = !isMuted
            volumeButton.setImage(UIImage(named: "Icon_Mute"), for: .normal)
        }
        player?.isMuted = isMuted
    }
    
    //MODIFIED:
    @objc func stopVideo() {
        if player?.rate != 0 {
            player?.pause()
        }
    }
    
    @objc func playVideo() {
        if player?.rate == 0 {
            player?.play()
        }
    }
    
    fileprivate func updateLike(post: Post) {
        let imageName = post.likes == nil || !post.isLiked! ? "like" : "likeSelected"
        likeImageView.image = UIImage(named: imageName)
        guard let count = post.likeCount else { return }
        if count != 0 {
            likeCountButton.setTitle("\(count) likes", for: .normal)
        } else {
            likeCountButton.setTitle("0 likes ", for: .normal)
        }
    }
    
    fileprivate func setupUserInfo() {
        fullNameLabel.text = user?.fullName
        if let profileUrlString = user?.profileImageUrl {
            let imageUrl = URL(string: profileUrlString)
            self.profileImageView.sd_setImage(with: imageUrl, placeholderImage: UIImage(named: "image2"))
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        fullNameLabel.text = ""
        captionLabel.text = ""
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleSelectCommentImageView))
        commentImageView.addGestureRecognizer(tapGesture)
        commentImageView.isUserInteractionEnabled = true
        
        let tapGestureForLikeImageView = UITapGestureRecognizer(target: self, action: #selector(handleSelectLikeImageView))
        likeImageView.addGestureRecognizer(tapGestureForLikeImageView)
        likeImageView.isUserInteractionEnabled = true
        
        let tapGestureForFullNameLabel = UITapGestureRecognizer(target: self, action: #selector(handleSelectFullNameLabel))
        fullNameLabel.addGestureRecognizer(tapGestureForFullNameLabel)
        fullNameLabel.isUserInteractionEnabled = true
    }
    
    @objc func handleSelectFullNameLabel() {
        if let id = user?.id {
            delegate?.goToProfileUserVC(userId: id)
        }
    }
    
    @objc func handleSelectLikeImageView() {
        guard let postId = post?.id else { return }
        Api.Post.incrementLikes(postId: postId, onSuccess: { (post) in
            self.updateLike(post: post)
            self.post?.likes = post.likes
            self.post?.isLiked = post.isLiked
            self.post?.likeCount = post.likeCount
            guard let currentUser = Api.User.CURRENT_USER else { return }
            if post.uid != currentUser.uid {
                let timestamp = NSNumber(value: Int(Date().timeIntervalSince1970))
                if post.isLiked! {
                    let newNotificationRef = Api.Notification.REF_NOTIFICATION.child(post.uid!).child("\(post.id!)-\(currentUser.uid)")
                    newNotificationRef.setValue(["from": currentUser.uid, "objectId": post.id!, "type": "like", "timestamp": timestamp])
                } else {
                    let newNotificationRef = Api.Notification.REF_NOTIFICATION.child(post.uid!).child("\(post.id!)-\(currentUser.uid)")
                    newNotificationRef.removeValue()
                }
            }
        }) { (error) in
            ProgressHUD.showError(error)
        }
    }
    
    @objc func handleSelectCommentImageView() {
        print("show comment")
        if let id = post?.id {
            delegate?.goToCommentVC(postId: id)
        }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        volumeView.isHidden = true
        profileImageView.image = UIImage(named: "image2")
        playerLayer?.removeFromSuperlayer()
        player?.pause()
        volumeView.isHidden = true
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
