//
//  CommentTableViewCell.swift
//  VeteranRealApp
//
//  Created by James Calby on 10/11/18.
//  Copyright © 2018 VeteranReal. All rights reserved.
//

import Foundation
import UIKit
import SDWebImage
import KILabel

protocol CommentTableViewCellDelegate {
    func goToProfileUserVC(userId: String)
    func goToHashTag(tag: String)
}

class CommentTableViewCell: UITableViewCell {
    
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var fullNameLabel: UILabel!
    @IBOutlet weak var commentLabel: KILabel!
    
    var delegate: CommentTableViewCellDelegate?
    
    var comment: Comment? {
        didSet {
            updateView()
        }
    }
    
    var user: UserModel? {
        didSet {
            setupUserInfo()
        }
    }
    
    fileprivate func updateView() {
        commentLabel.text = comment?.commentText
        commentLabel.hashtagLinkTapHandler = { label, string, range in
            let tag = String(string.dropFirst())
            self.delegate?.goToHashTag(tag: tag)
        }
        
        commentLabel.userHandleLinkTapHandler = { label, string, range in
            let mention = String(string.dropFirst())
            Api.User.observeUserByFullName(fullName: mention.lowercased(), completion: { (user) in
                guard let userId = user.id else { return }
                self.delegate?.goToProfileUserVC(userId: userId)
            })
        }
    }
    
    fileprivate func setupUserInfo() {
        fullNameLabel.text = user?.fullName
        guard let profileUrlString = user?.profileImageUrl else { return }
        let imageUrl = URL(string: profileUrlString)
        profileImageView.sd_setImage(with: imageUrl, placeholderImage: UIImage(named: "image2"))
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        fullNameLabel.text = ""
        commentLabel.text = ""
        
        let tapGestureForFullNameLabel = UITapGestureRecognizer(target: self, action: #selector(handleSelectFullNameLabel))
        fullNameLabel.addGestureRecognizer(tapGestureForFullNameLabel)
        fullNameLabel.isUserInteractionEnabled = true
    }
    
    @objc func handleSelectFullNameLabel() {
        if let id = user?.id {
            delegate?.goToProfileUserVC(userId: id)
        }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        profileImageView.image = UIImage(named: "image2")
    }
}
