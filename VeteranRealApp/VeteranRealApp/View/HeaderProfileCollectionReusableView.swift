//
//  HeaderProfileCollectionReusableView.swift
//  VeteranRealApp
//
//  Created by James Calby on 10/11/18.
//  Copyright © 2018 VeteranReal. All rights reserved.
//

import Foundation
import UIKit

protocol HeaderProfileCollectionReusableViewDelegate {
    func updateFollowButton(forUser user: UserModel)
}

protocol HeaderProfileCollectionReusableViewDelegateSwitchSettingVC {
    func goToSettingVC()
}
class HeaderProfileCollectionReusableView: UICollectionReusableView {
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var fullNameLabel: UILabel!
    @IBOutlet weak var postCountLabel: UILabel!
    @IBOutlet weak var followingCountLabel: UILabel!
    @IBOutlet weak var followersCountLabel: UILabel!
    @IBOutlet weak var followButton: UIButton!
    
    var delegate: HeaderProfileCollectionReusableViewDelegate?
    
    var goToSettingVCDelegate: HeaderProfileCollectionReusableViewDelegateSwitchSettingVC?
    
    var user: UserModel? {
        didSet {
            updateView()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        clear()
    }
    func updateView() {
        guard let user = user else { return }
        self.fullNameLabel.text = user.fullName
        guard let photoUrlString = user.profileImageUrl else { return }
        let photoUrl = URL(string: photoUrlString)
        self.profileImageView.sd_setImage(with: photoUrl, completed: nil)
        
        guard let userId = user.id else { return }
        Api.MyPosts.fetchCountMyPosts(userId: userId) { (count) in
            self.postCountLabel.text = "\(count)"
        }
        
        Api.Follow.fetchCountFollowing(userId: userId) { (count) in
            self.followingCountLabel.text = "\(count)"
        }
        
        Api.Follow.fetchCountFollowers(userId: userId) { (count) in
            self.followersCountLabel.text = "\(count)"
        }
        
        if user.id == Api.User.CURRENT_USER?.uid {
            followButton.setTitle("Edit Profile", for: .normal)
            followButton.addTarget(self, action: #selector(handleGoToSettingVC), for: .touchUpInside)
        } else {
            updateStateFollowButton()
        }
    }
    
    func clear() {
        self.fullNameLabel.text = ""
        self.postCountLabel.text = ""
        self.followingCountLabel.text = ""
        self.followersCountLabel.text = ""
    }
    
    @objc func handleGoToSettingVC() {
        goToSettingVCDelegate?.goToSettingVC()
    }
    
    fileprivate func updateStateFollowButton() {
        guard let isFollowing = user?.isFollowing else { return }
        if isFollowing {
            configureUnFollowButton()
        } else {
            configureFollowButton()
        }
    }
    
    func configureFollowButton() {
        followButton.layer.borderWidth = 1
        followButton.layer.borderColor = UIColor(red: 226/255, green: 228/255, blue: 232/255, alpha: 1).cgColor
        followButton.layer.cornerRadius = 5
        followButton.clipsToBounds = true
        followButton.setTitleColor(.white, for: .normal)
        followButton.backgroundColor = .black
        
        self.followButton.setTitle("Follow", for: .normal)
        followButton.addTarget(self, action: #selector(followAction), for: .touchUpInside)
    }
    
    func configureUnFollowButton() {
        followButton.layer.borderWidth = 1
        followButton.layer.borderColor = UIColor(red: 226/255, green: 228/255, blue: 232/255, alpha: 1).cgColor
        followButton.layer.cornerRadius = 5
        followButton.clipsToBounds = true
        followButton.setTitleColor(.white, for: .normal)
        followButton.backgroundColor = UIColor(red: 69/255, green: 142/255, blue: 255/255, alpha: 1)
        
        self.followButton.setTitle("Following", for: .normal)
        followButton.addTarget(self, action: #selector(unFollowAction), for: .touchUpInside)
        
    }
    
    @objc func followAction() {
        if user?.isFollowing == false {
            guard let userId = user?.id else { return }
            Api.Follow.followAction(withUser: userId)
            configureUnFollowButton()
            user?.isFollowing = true
            guard let user = user else { return }
            delegate?.updateFollowButton(forUser: user)
        }
    }
    
    @objc func unFollowAction() {
        if user?.isFollowing == true {
            guard let userId = user?.id else { return }
            Api.Follow.unFollowAction(withUser: userId)
            configureFollowButton()
            user?.isFollowing = false
            guard let user = user else { return }
            delegate?.updateFollowButton(forUser: user)
        }
    }
}
