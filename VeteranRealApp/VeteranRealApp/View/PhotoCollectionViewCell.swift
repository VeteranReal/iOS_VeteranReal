//
//  PhotoCollectionViewCell.swift
//  VeteranRealApp
//
//  Created by James Calby on 10/11/18.
//  Copyright © 2018 VeteranReal. All rights reserved.
//

import Foundation
import UIKit

protocol PhotoCollectionViewCellDelegate {
    func goToDetailVC(postId: String)
}
class PhotoCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var photoImageView: UIImageView!
    
    var delegate: PhotoCollectionViewCellDelegate?
    
    var post: Post? {
        didSet {
            updateView()
        }
    }
    
    fileprivate func updateView() {
        guard let photoUrlString = post?.photoUrl else { return }
        let photoUrl = URL(string: photoUrlString)
        photoImageView.sd_setImage(with: photoUrl, completed: nil)
        
        let tapGestureForPhotoImageView = UITapGestureRecognizer(target: self, action: #selector(handleSelectPhotoImageView))
        photoImageView.addGestureRecognizer(tapGestureForPhotoImageView)
        photoImageView.isUserInteractionEnabled = true
    }
    
    @objc func handleSelectPhotoImageView() {
        if let postId = post?.id {
            delegate?.goToDetailVC(postId: postId)
        }
    }
}
