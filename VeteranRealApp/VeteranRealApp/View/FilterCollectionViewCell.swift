//
//  FilterCollectionViewCell.swift
//  VeteranRealApp
//
//  Created by James Calby on 10/11/18.
//  Copyright © 2018 VeteranReal. All rights reserved.
//

import Foundation
import UIKit

class FilterCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var filterImage: UIImageView!
}
