//
//  ColorExtension.swift
//  VeteranRealApp
//
//  Created by James Calby on 10/14/18.
//  Copyright © 2018 VeteranReal. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
    
    static func rgb(red: CGFloat, green: CGFloat, blue: CGFloat) -> UIColor{
        return UIColor(red: red/255, green: green/255, blue:blue/255, alpha: 1)
    }
}
