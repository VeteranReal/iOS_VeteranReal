//
//  ViewExtension.swift
//  VeteranRealApp
//
//  Created by James Calby on 10/14/18.
//  Copyright © 2018 VeteranReal. All rights reserved.
//

import Foundation
import UIKit

extension UIView{
    
    func addConstraintsWithFormat(format:String, views:UIView...) {
        var viewsDictionary = [String: UIView]()
        for(index, view) in views.enumerated(){
            let key = "v\(index)"
            viewsDictionary[key] = view
            view.translatesAutoresizingMaskIntoConstraints = false
        }
        
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: format, options: NSLayoutConstraint.FormatOptions(), metrics: nil, views: viewsDictionary))
    }
}
