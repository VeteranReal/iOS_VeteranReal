//
//  WelcomeViewController.swift
//  VeteranRealApp
//
//  Created by James Calby on 10/14/18.
//  Copyright © 2018 VeteranReal. All rights reserved.
//

import UIKit

class WelcomeViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    override func viewDidAppear(_ animated: Bool) {
        
        // Run the walkthrough
         super.viewDidAppear(animated)
         let defaults = UserDefaults.standard
         let hasViewdWalkthrough = defaults.bool(forKey: "hasViewedWalkthrough")
         if !hasViewdWalkthrough {
             if let pageVC = storyboard?.instantiateViewController(withIdentifier: "WalkthroughViewController") as? WalkthroughViewController {
                 present(pageVC, animated: true, completion: nil)
             }
         }
        
        // if Api.User.CURRENT_USER != nil {
        //     self.performSegue(withIdentifier: "welcomeToTabbarVC", sender: nil)
        // }
    }

}
