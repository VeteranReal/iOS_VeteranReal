//
//  OrgViewController.swift
//  VeteranRealApp
//
//  Created by James Calby on 10/16/18.
//  Copyright © 2018 VeteranReal. All rights reserved.
//

import Foundation
import UIKit
import MessageUI

class OrgViewController: UIViewController, MFMailComposeViewControllerDelegate{
    
    
    @IBOutlet weak var orgName: UITextField!
    @IBOutlet weak var OrgEmail: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func sendEmailButtonTapped(_ sender: Any) {
        let mailComposeViewController = configuredMailComposeViewController()
        if MFMailComposeViewController.canSendMail(){
            self.present(mailComposeViewController, animated: true, completion: nil)
        } else {
            self.showSendMailErrorAlert()
        }
        
    }
    
    func configuredMailComposeViewController() -> MFMailComposeViewController {
        let mailComposerVC = MFMailComposeViewController()
        mailComposerVC.mailComposeDelegate = self
        mailComposerVC.setToRecipients(["orgrequest@veteranreal.org"])
        mailComposerVC.setSubject(" Organization Access Request eMail")
        mailComposerVC.setMessageBody("Please Contact me in Reference to Creating an Orginization Account on Veteran Real App. Please Enter Org Contact Information Below. Please Include - Organization Name, Organization Email, Phone Number and Name of Person Requesting Assess", isHTML: false)
        // self.performSegue(withIdentifier: "welcomeToTabbarVC", sender: nil)
        return mailComposerVC
    }
    
    // to alert user they need an active email and client to senfd email
    func showSendMailErrorAlert() {
        let sendMailErrorAlert = UIAlertView(title: "Could not send Email", message: "Your device must have a active Emil account", delegate: self, cancelButtonTitle: "OK")
        sendMailErrorAlert.show()
        
    }
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
    
    
    
    
    
}
