//
//  SignInViewController.swift
//  VeteranRealApp
//
//  Created by James Calby on 10/11/18.
//  Copyright © 2018 VeteranReal. All rights reserved.
//

import Foundation
import UIKit

class SignInViewController: UIViewController {
    
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var logInButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        emailTextField.backgroundColor = .clear
        emailTextField.attributedPlaceholder = NSAttributedString(string: emailTextField.placeholder ?? "", attributes: [NSAttributedString.Key.foregroundColor : UIColor(white: 1, alpha: 0.6)])
        emailTextField.tintColor = .white
        emailTextField.textColor = .white
        let bottomLayerEmail = CALayer()
        bottomLayerEmail.frame = CGRect(x: 0, y: 39, width: 1000, height: 0.6)
        bottomLayerEmail.backgroundColor = UIColor(red: 50/255, green: 50/255, blue: 50/255, alpha: 1).cgColor
        emailTextField.layer.addSublayer(bottomLayerEmail)
        
        passwordTextField.backgroundColor = .clear
        passwordTextField.attributedPlaceholder = NSAttributedString(string: passwordTextField.placeholder ?? "", attributes: [NSAttributedString.Key.foregroundColor : UIColor(white: 1, alpha: 0.6)])
        passwordTextField.tintColor = .white
        passwordTextField.textColor = .white
        let bottomLayerPassword = CALayer()
        bottomLayerPassword.frame = CGRect(x: 0, y: 39, width: 1000, height: 0.6)
        bottomLayerPassword.backgroundColor = UIColor(red: 50/255, green: 50/255, blue: 50/255, alpha: 1).cgColor
        passwordTextField.layer.addSublayer(bottomLayerPassword)
        logInButton.isEnabled = false
        handleTextField()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    
    
    //
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
       // let defaults = UserDefaults.standard
       //  let hasViewdWalkthrough = defaults.bool(forKey: "hasViewedWalkthrough")
       //  if !hasViewdWalkthrough {
       //      if let pageVC = storyboard?.instantiateViewController(withIdentifier: "WalkthroughViewController") as? WalkthroughViewController {
       //          present(pageVC, animated: true, completion: nil)
       //      }
      //   }
        if Api.User.CURRENT_USER != nil {
            self.performSegue(withIdentifier: "signInToTabbarVC", sender: nil)
        }
    }
    
    func handleTextField() {
        emailTextField.addTarget(self, action: #selector(handleTextFieldDidChange), for: .editingChanged)
        passwordTextField.addTarget(self, action: #selector(handleTextFieldDidChange), for: .editingChanged)
    }
    
    @objc func handleTextFieldDidChange() {
        guard let email = emailTextField.text, !email.isEmpty, let password = passwordTextField.text, !password.isEmpty else {
            logInButton.setTitleColor(.lightGray, for: .normal)
            logInButton.isEnabled = false
            return
        }
        logInButton.isEnabled = true
        logInButton.setTitleColor(.white, for: .normal)
    }
    
    @IBAction func logInButton(_ sender: Any) {
        view.endEditing(true)
        ProgressHUD.show("Waiting....", interaction: false)
        guard let email = emailTextField.text else { return }
        guard let password = passwordTextField.text else { return }
        AuthService.signIn(email: email, password: password, onSuccess: {
            ProgressHUD.showSuccess("Success")
            self.performSegue(withIdentifier: "signInToTabbarVC", sender: nil)
        }) { (error) in
            ProgressHUD.showError(error)
        }
    }
}
