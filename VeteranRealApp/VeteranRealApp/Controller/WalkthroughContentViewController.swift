//
//  WalkthroughContentViewController.swift
//  VeteranRealApp
//
//  Created by James Calby on 10/11/18.
//  Copyright © 2018 VeteranReal. All rights reserved.
//

import Foundation
import UIKit

class WalkthroughContentViewController: UIViewController {
    
    @IBOutlet weak var backgroundImageView: UIImageView!
    @IBOutlet weak var contentLabel: UILabel!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var nextButton: UIButton!
    
    var index = 0
    var imageFileName = ""
    var content = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        contentLabel.text = content
        backgroundImageView.image = UIImage(named: imageFileName)
        pageControl.currentPage = index
        switch index {
        case 0...1:
            nextButton.setImage(UIImage(named: "arrow"), for: .normal)
        case 2:
            nextButton.setImage(UIImage(named: "arrow"), for: .normal)
        default:
            break
        }
    }
    
    @IBAction func nextButtonDidTapped(_ sender: Any) {
        switch index {
        case 0...1:
            let pageVC = parent as! WalkthroughViewController
            pageVC.next(index: index)
        case 2:
            let defaults = UserDefaults.standard
            defaults.set(true, forKey: "hasViewedWalkthrough")
            dismiss(animated: true, completion: nil)
        default:
            print("hasViewedWalkthrough")
        }
    }
}

