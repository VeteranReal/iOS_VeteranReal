//
//  CameraViewController.swift
//  VeteranRealApp
//
//  Created by James Calby on 10/11/18.
//  Copyright © 2018 VeteranReal. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation
import ImagePicker

class CameraViewController: UIViewController, ImagePickerDelegate {
    
    @IBOutlet weak var photoImageView: UIImageView!
    @IBOutlet weak var captionTextView: UITextView!
    @IBOutlet weak var shareButton: UIButton!
    @IBOutlet weak var removeButton: UIBarButtonItem!
    
    var selectedImage: UIImage?
    var videoUrl: URL?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleSelectPhotoImageView))
        photoImageView.addGestureRecognizer(tapGesture)
        photoImageView.isUserInteractionEnabled = true
    }
    
    @objc func handleSelectPhotoImageView() {
        let pickerController = UIImagePickerController()
        pickerController.delegate = self
        pickerController.mediaTypes = ["public.image","public.movie"]
        pickerController.allowsEditing = true
        present(pickerController, animated: true, completion: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        handlePost()
    }
    
    func handlePost() {
        if selectedImage != nil {
            shareButton.isEnabled = true
            removeButton.isEnabled = true
            shareButton.backgroundColor = .black
        } else {
            shareButton.isEnabled = false
            removeButton.isEnabled = false
            shareButton.backgroundColor = .lightGray
        }
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    
    @IBAction func shareButtonDidTapped(_ sender: Any) {
        view.endEditing(true)
        ProgressHUD.show("Waiting....", interaction: false)
        if let image = self.selectedImage, let imageData = image.jpegData(compressionQuality: 0.1) {
            let ratio = image.size.width / image.size.height
            guard let caption = captionTextView.text else { return }
            HelperService.uploadDataToStorage(data: imageData, videoUrl: self.videoUrl, ratio: ratio, caption: caption) {
                self.clearPostInputs()
                self.tabBarController?.selectedIndex = 0
            }
        } else {
            ProgressHUD.showError("Profile image can't be empty")
        }
    }
    
    @IBAction func cameraButtonDidTapped(_ sender: Any) {
        let imagePickerController = ImagePickerController()
        imagePickerController.delegate = self
        imagePickerController.imageLimit = 1
        present(imagePickerController, animated: true, completion: nil)
        
    }
    
    func wrapperDidPress(_ imagePicker: ImagePickerController, images: [UIImage]) {
        print("wrapper")
    }
    
    func doneButtonDidPress(_ imagePicker: ImagePickerController, images: [UIImage]) {
        guard let image = images.first else {
            dismiss(animated: true, completion: nil)
            return
        }
        selectedImage = image
        photoImageView.image = image
        dismiss(animated: true) {
            self.performSegue(withIdentifier: "FilterSegue", sender: nil)
        }
    }
    
    func cancelButtonDidPress(_ imagePicker: ImagePickerController) {
        print("cancel")
    }
    
    fileprivate func clearPostInputs() {
        self.captionTextView.text = ""
        self.photoImageView.image = UIImage(named: "Placeholder-image")
        self.selectedImage = nil
    }
    
    @IBAction func removeButtonDidTapped(_ sender: Any) {
        clearPostInputs()
        handlePost()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "FilterSegue" {
            let filterVC = segue.destination as! FilterViewController
            filterVC.selectedImage = self.selectedImage
            filterVC.delegate = self
        }
    }
}

extension CameraViewController : UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        // Local variable inserted by Swift 4.2 migrator.
        let info = convertFromUIImagePickerControllerInfoKeyDictionary(info)
        
        print("did Finish Picking Media")
        print(info)
        if let videoUrl = info["UIImagePickerControllerMediaURL"] as? URL, let thumbnailImage = self.thumbnailImageForFileUrl(videoUrl) {
            selectedImage = thumbnailImage
            photoImageView.image = thumbnailImage
            self.videoUrl = videoUrl
            dismiss(animated: true, completion: nil)
        }
        
        if let image = info["UIImagePickerControllerOriginalImage"] as? UIImage {
            selectedImage = image
            photoImageView.image = image
            dismiss(animated: true) {
                self.performSegue(withIdentifier: "FilterSegue", sender: nil)
            }
        }
    }
    
    func thumbnailImageForFileUrl(_ fileUrl: URL) -> UIImage? {
        let asset = AVAsset(url: fileUrl)
        let imageGenerator = AVAssetImageGenerator(asset: asset)
        imageGenerator.appliesPreferredTrackTransform = true
        do {
            let thumbnailCGImage = try imageGenerator.copyCGImage(at: CMTimeMake(value: 7, timescale: 1), actualTime: nil)
            return UIImage(cgImage: thumbnailCGImage)
        } catch let err {
            print(err)
        }
        return nil
    }
}

extension CameraViewController: FilterViewControllerDelegate {
    func updatePhoto(image: UIImage) {
        self.photoImageView.image = image
        self.selectedImage = image
    }
}






// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKeyDictionary(_ input: [UIImagePickerController.InfoKey: Any]) -> [String: Any] {
    return Dictionary(uniqueKeysWithValues: input.map {key, value in (key.rawValue, value)})
}
