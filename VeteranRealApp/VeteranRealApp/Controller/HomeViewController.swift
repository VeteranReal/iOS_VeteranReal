//
//  HomeViewController.swift
//  VeteranRealApp
//
//  Created by James Calby on 10/11/18.
//  Copyright © 2018 VeteranReal. All rights reserved.
//

import Foundation
import UIKit
import ImagePicker
import Firebase

class HomeViewController: UIViewController, ImagePickerDelegate {
    
    private let hasAlreadyLaunchedKey = "HasAlredyLaunched"
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var activityIndicatorView: UIActivityIndicatorView!
    
    let refreshControl = UIRefreshControl()
    
    var posts = [Post]()
    var users = [UserModel]()
    
    var isLoadingPost = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.estimatedRowHeight = 521
        tableView.rowHeight = UITableView.automaticDimension
        tableView.dataSource = self
        tableView.delegate = self
        refreshControl.addTarget(self, action:#selector(refresh),for: .valueChanged)
        tableView.addSubview(refreshControl)
        activityIndicatorView.startAnimating()
        loadPosts()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if !UserDefaults.standard.bool(forKey: self.hasAlreadyLaunchedKey)
        {
            UserDefaults.standard.set(true, forKey: self.hasAlreadyLaunchedKey)
            let alertController = UIAlertController(title: "Create a Post", message: "Select the POST on the top right corner of the iPhone to start added to your Home Screen", preferredStyle: .alert)
            let alertAction = UIAlertAction.init(title: "OK", style: UIAlertAction.Style.default, handler: { (action:UIAlertAction) in
            })
            alertController.addAction(alertAction)
            
            self.present(alertController, animated: true, completion: nil)
            
        }
    }
    
    @objc func refresh() {
        posts.removeAll()
        users.removeAll()
        loadPosts()
    }
    
    func loadPosts() {
        guard let currentUser = Api.User.CURRENT_USER else { return }
        isLoadingPost = true
        Api.Feed.getRecentFeed(withId: currentUser.uid, start: posts.first?.timestamp, limit: 3) { (results) in
            if !results.isEmpty {
                results.forEach({ (result) in
                    self.posts.append(result.0)
                    self.users.append(result.1)
                })
            }
            if self.refreshControl.isRefreshing {
                self.refreshControl.endRefreshing()
            }
            self.isLoadingPost = false
            self.activityIndicatorView.stopAnimating()
            self.tableView.reloadData()
        }
        Api.Feed.observeFeedRemoved(withId: currentUser.uid) { (post) in
            self.posts = self.posts.filter { $0.id != post.id }
            self.users = self.users.filter { $0.id != post.uid }
            self.tableView.reloadData()
        }
        
    }
    
    private func displayNewPosts(newPosts posts: [Post]) {
        guard posts.count > 0 else {
            return
        }
        var indexPaths:[IndexPath] = []
        self.tableView.beginUpdates()
        for post in 0...(posts.count - 1) {
            let indexPath = IndexPath(row: post, section: 0)
            indexPaths.append(indexPath)
        }
        self.tableView.insertRows(at: indexPaths, with: .none)
        self.tableView.endUpdates()
    }
    
    func loadMore() {
        guard let currentUser = Api.User.CURRENT_USER else { return }
        guard !isLoadingPost else { return }
        isLoadingPost = true
        guard let lastLatestPostTimestamp = self.posts.last?.timestamp else {
            isLoadingPost = false
            return
        }
        Api.Feed.getOldFeed(withId: currentUser.uid, start: lastLatestPostTimestamp, limit: 3) { (results) in
            if results.isEmpty {
                return
            }
            for result in results {
                self.posts.append(result.0)
                self.users.append(result.1)
            }
            self.tableView.reloadData()
            self.isLoadingPost = false
        }
    }
    
    @IBAction func cameraButtonDidTapped(_ sender: Any) {
        let imagePickerController = ImagePickerController()
        imagePickerController.delegate = self
        imagePickerController.imageLimit = 1
        present(imagePickerController, animated: true, completion: nil)
    }
    
    func wrapperDidPress(_ imagePicker: ImagePickerController, images: [UIImage]) {
        print("wrapper")
    }
    
    func doneButtonDidPress(_ imagePicker: ImagePickerController, images: [UIImage]) {
        guard let image = images.first else {
            dismiss(animated: true, completion: nil)
            return
        }
        ProgressHUD.show("Waiting....", interaction: false)
        if let imageData = image.jpegData(compressionQuality: 0.1) {
            let ratio = image.size.width / image.size.height
            HelperService.uploadDataToStorage(data: imageData, videoUrl: nil, ratio: ratio, caption: "") {
                self.dismiss(animated: true, completion: nil)
                self.posts.removeAll()
                self.users.removeAll()
                self.loadPosts()
            }
        } else {
            ProgressHUD.showError("Profile image can't be empty")
        }
    }
    
    func cancelButtonDidPress(_ imagePicker: ImagePickerController) {
        print("cancel")
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "CommentSegue" {
            let commentVC = segue.destination as! CommentViewController
            let postId = sender as! String
            commentVC.postId = postId
        }
        
        if segue.identifier == "HomeToProfileSegue" {
            let profileVC = segue.destination as! ProfileUserViewController
            let userId = sender as! String
            profileVC.userId = userId
        }
        
        if segue.identifier == "HomeToHashTagSegue" {
            let hashTagVC = segue.destination as! HashTagViewController
            let tag = sender as! String
            hashTagVC.tag = tag
        }
    }
}

extension HomeViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let cell = tableView.cellForRow(at: indexPath) as! HomeTableViewCell
        let deleteAction = UITableViewRowAction(style: UITableViewRowAction.Style.default, title: "Delete") { (action:UITableViewRowAction, indexPath: IndexPath) in
            if let postKey = cell.post?.id {
                Api.MyPosts.REF_MY_POSTS.child(postKey).removeValue()
                Api.Post.REF_POSTS.child(postKey).removeValue()
                Api.Follow.REF_FOLLOWING.child(Api.User.CURRENT_USER!.uid).observeSingleEvent(of: .value, with: { (snapshot) in
                    let arraySnapshot = (snapshot.children.allObjects as! [DataSnapshot]).reversed()
                    arraySnapshot.forEach({ (child) in
                        Api.Feed.REF_FEED.child(child.key).child(postKey).removeValue()
                    })
                })
                self.posts.remove(at: indexPath.row)
                self.users.remove(at: indexPath.row)
                tableView.deleteRows(at: [indexPath], with: .fade)
            }
            
        }
        
        if cell.user!.id! == Api.User.CURRENT_USER!.uid {
            return [deleteAction]
        }
        return []
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.contentOffset.y >= scrollView.contentSize.height - self.view.frame.size.height {
            loadMore()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.post(name: NSNotification.Name.init("stopVideo"), object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NotificationCenter.default.post(name: NSNotification.Name.init("playVideo"), object: nil)
    }
}

extension HomeViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if posts.isEmpty {
            return 0
        }
        return posts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PostCell", for: indexPath) as! HomeTableViewCell
        if posts.isEmpty {
            return UITableViewCell()
        }
        
        cell.post = posts[indexPath.row]
        cell.user = users[indexPath.row]
        cell.delegate = self
        return cell
    }
}

extension HomeViewController: HomeTableViewCellDelegate {
    func goToCommentVC(postId: String) {
        performSegue(withIdentifier: "CommentSegue", sender: postId)
    }
    
    func goToProfileUserVC(userId: String) {
        performSegue(withIdentifier: "HomeToProfileSegue", sender: userId)
    }
    
    func goToHashTag(tag: String) {
        performSegue(withIdentifier: "HomeToHashTagSegue", sender: tag)
    }
}

