//
//  NotificationViewController.swift
//  VeteranRealApp
//
//  Created by James Calby on 10/11/18.
//  Copyright © 2018 VeteranReal. All rights reserved.
//

import Foundation
import UIKit

class NotificationViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var notifications = [NotificationModel]()
    var users = [UserModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = self
        
        loadNotifications()
    }
    
    func loadNotifications() {
        guard let currentUser = Api.User.CURRENT_USER else { return }
        Api.Notification.observeNotification(withId: currentUser.uid) { (notification) in
            guard let uid = notification.from else { return }
            self.fetchUser(uid: uid, completed: {
                self.notifications.insert(notification, at: 0)
                self.tableView.reloadData()
            })
        }
    }
    
    fileprivate func fetchUser(uid: String, completed: @escaping () -> ()) {
        Api.User.observeUser(withId: uid) { (user) in
            self.users.insert(user, at: 0)
            completed()
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ActivityToDetailSegue" {
            let detailVC = segue.destination as! DetailViewController
            let postId = sender as! String
            detailVC.postId = postId
        }
        if segue.identifier == "ActivityToProfileSegue" {
            let profileVC = segue.destination as! ProfileUserViewController
            let userId = sender  as! String
            profileVC.userId = userId
        }
        if segue.identifier == "ActivityToCommentSegue" {
            let commentVC = segue.destination as! CommentViewController
            let postId = sender  as! String
            commentVC.postId = postId
        }
    }
    
}

extension NotificationViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return notifications.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ActivityTableViewCell", for: indexPath) as! ActivityTableViewCell
        cell.notification = notifications[indexPath.row]
        cell.user = users[indexPath.row]
        cell.delegate = self
        return cell
    }
    
}

extension NotificationViewController: ActivityTableViewCellDelegate {
    func goToDetailVC(postId: String) {
        performSegue(withIdentifier: "ActivityToDetailSegue", sender: postId)
    }
    func goToProfileVC(userId: String) {
        performSegue(withIdentifier: "ActivityToProfileSegue", sender: userId)
    }
    func goToCommentVC(postId: String) {
        performSegue(withIdentifier: "ActivityToCommentSegue", sender: postId)
    }
}

