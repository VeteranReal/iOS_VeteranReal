//
//  CommentViewController.swift
//  VeteranRealApp
//
//  Created by James Calby on 10/11/18.
//  Copyright © 2018 VeteranReal. All rights reserved.
//

import Foundation
import UIKit

class CommentViewController: UIViewController {
    
    @IBOutlet weak var commentTextField: UITextField!
    @IBOutlet weak var sendButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var constraintToBottom: NSLayoutConstraint!
    
    var postId: String!
    var comments = [Comment]()
    var users = [UserModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Comment"
        tableView.dataSource = self
        tableView.estimatedRowHeight = 81
        tableView.rowHeight = UITableView.automaticDimension
        clearCommentInputs()
        handleTextField()
        loadComments()
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        print(notification)
        let keyboardFrame = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as AnyObject).cgRectValue
        UIView.animate(withDuration: 0.3) {
            self.constraintToBottom.constant = -(keyboardFrame?.height ?? 0)
            self.view.layoutIfNeeded()
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        print(notification)
        self.constraintToBottom.constant = 0
        self.view.layoutIfNeeded()
    }
    
    fileprivate func loadComments() {
        Api.Posts_Comments.observePostsComments(withPostId: self.postId) { (snapshot) in
            Api.Comment.observeComments(withPostId: snapshot.key, completion: { (comment) in
                guard let uid = comment.uid else { return }
                self.fetchUser(uid: uid, completed: {
                    self.comments.append(comment)
                    self.tableView.reloadData()
                })
            })
        }
    }
    
    fileprivate func fetchUser(uid: String, completed: @escaping () -> ()) {
        Api.User.observeUser(withId: uid) { (user) in
            self.users.append(user)
            completed()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.tabBarController?.tabBar.isHidden = false
    }
    
    func handleTextField() {
        commentTextField.addTarget(self, action: #selector(handleTextFieldDidChange), for: .editingChanged)
    }
    
    @objc func handleTextFieldDidChange() {
        if let comment = commentTextField.text, !comment.isEmpty {
            sendButton.isEnabled = true
            sendButton.setTitleColor(.black, for: .normal)
            return
        }
        sendButton.setTitleColor(.lightGray, for: .normal)
        sendButton.isEnabled = false
    }
    
    @IBAction func sendButton(_ sender: Any) {
        guard let currentUser = Api.User.CURRENT_USER else { return }
        guard let comment = commentTextField.text else { return }
        let commentRef = Api.Comment.REF_COMMENTS
        let commentId = commentRef.childByAutoId().key
        // let newCommentRef = commentRef.child(commentId)
        let newCommentRef = commentRef.child(commentId!)
        let values = ["uid" : currentUser.uid, "commentText" : comment] as [String : Any]
        newCommentRef.setValue(values, withCompletionBlock: { (err, _) in
            if let err = err {
                ProgressHUD.showError(err.localizedDescription)
                return
            }
            let words = comment.components(separatedBy: CharacterSet.whitespacesAndNewlines)
            for var word in words {
                if word.hasPrefix("#") {
                    word = word.trimmingCharacters(in: CharacterSet.punctuationCharacters)
                    let newHashTagRef = Api.HashTag.REF_HASHTAG.child(word.lowercased())
                    newHashTagRef.updateChildValues([self.postId!: true])
                }
            }
            
            // let postCommentRef = Api.Posts_Comments.REF_POSTS_COMMENTS.child(self.postId).child(commentId)
            let postCommentRef = Api.Posts_Comments.REF_POSTS_COMMENTS.child(self.postId).child(commentId!)
            postCommentRef.setValue(true, withCompletionBlock: { (error, _) in
                if let err = error {
                    ProgressHUD.showError(err.localizedDescription)
                    return
                }
                Api.Post.observePost(withId: self.postId, completion: { (post) in
                    if post.uid != currentUser.uid {
                        let timestamp = NSNumber(value: Int(Date().timeIntervalSince1970))
                        let newNotificationId = Api.Notification.REF_NOTIFICATION.child(post.uid!).childByAutoId().key
                        // let newNotificationRef = Api.Notification.REF_NOTIFICATION.child(post.uid!).child(newNotificationId)
                        let newNotificationRef = Api.Notification.REF_NOTIFICATION.child(post.uid!).child(newNotificationId!)
                        newNotificationRef.setValue(["from": currentUser.uid, "objectId": self.postId!, "type": "comment", "timestamp": timestamp])
                    }
                })
            })
            self.clearCommentInputs()
            self.view.endEditing(true)
        })
    }
    
    fileprivate func clearCommentInputs() {
        self.commentTextField.text = ""
        sendButton.isEnabled = false
        sendButton.setTitleColor(.lightGray, for: .normal)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "CommentToProfileSegue" {
            let profileVC = segue.destination as! ProfileUserViewController
            let userId = sender as! String
            profileVC.userId = userId
        }
        
        if segue.identifier == "CommentToHashTagSegue" {
            let hashTagVC = segue.destination as! HashTagViewController
            let tag = sender as! String
            hashTagVC.tag = tag
        }
    }
}

extension CommentViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return comments.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CommentCell", for: indexPath) as! CommentTableViewCell
        cell.comment = comments[indexPath.row]
        cell.user = users[indexPath.row]
        cell.delegate = self
        return cell
    }
}

extension CommentViewController: CommentTableViewCellDelegate {
    func goToProfileUserVC(userId: String) {
        performSegue(withIdentifier: "CommentToProfileSegue", sender: userId)
    }
    
    func goToHashTag(tag: String) {
        performSegue(withIdentifier: "CommentToHashTagSegue", sender: tag)
    }
}
